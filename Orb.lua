Core={
	LoopKill={};
	keys={};
	Banned={
		{Name="locococo01"};
		{Name="TycoonKing1981"};
        {Name="P_ornhubMakesmecum"};
        {Name="YourTrueEnd"};
        {Name="ScriptEdit0R"};
        {Name="RAFA1608"};
        {Name="cthuloid9"};
        {Name="TheWeabooo"};
	};
	Log={};
	Kicked = {};
	Vars={
		model=nil;
		Torso=nil;
		Outside=nil;
		txt=nil
	};
	Settings={
		Owner = game.Players.LocalPlayer;
		Name = game.Players.LocalPlayer.Name;
		scriptLock=false;
		localSciptLock=false;
		Mouse = nil;
		Camera = workspace.CurrentCamera;
		speed=0.5;
		speedafk=0.005;
		BetaKey="/";
	};
	Commands={};
	CoreFunctions={
		scriptLock=function(plr)
        	plr.Character.ChildAdded:connect(function(instance)
        		if instance.ClassName == "Script" and Core.Settings.scriptLock == true then
        		    instance.Disabled=true
        		    instance.Parent=game.Lighting
        			instance:remove()
        		end
            end)
    	    workspace.ChildAdded:connect(function(instance)
	            if instance.ClassName == "Script" and Core.Settings.scriptLock == true then
	                instance.Disabled=true
	                instance.Parent=game.Lighting
			        instance:remove()
    	    	end
            end)
            workspace.ChildRemoved:connect(function(instance)
                for i,v in pairs(game.Players:GetPlayers()) do
                    if v.Name == instance.Name then
                        Core.CoreFunctions.scriptLock(v)
                    end
                end
            end)
        end;
        localScriptLock=function(plr)
        	plr.Character.ChildAdded:connect(function(instance)
        		if instance.ClassName == "LocalScript" and Core.Settings.localScriptLock == true then
        		    AddPart("Local Scripts are locked","Really red",plr)
    	            instance.Disabled=true
    	            instance.Parent=game.Lighting
        			instance:remove()
        		end
	        end)
    	    workspace.ChildAdded:connect(function(instance)
    	        if instance.ClassName == "LocalScript" and Core.Settings.localScriptLock == true then
    	            instance.Disabled=true
    	            instance.Parent=game.Lighting
    			    instance:remove()
    		    end
            end)
            workspace.ChildRemoved:connect(function(instance)
                for i,v in pairs(game.Players:GetPlayers()) do
                    if v.Name == instance.Name then
                        Core.CoreFunctions.localScriptLock(v)
                    end
                end
            end)
        end;
		OnChatted=function(Msg)
        if not Msg or type(Msg) ~= "string" then return end
        Core.Vars.txt.Text = Core.Settings.Name.." : "..Msg
		game:GetService("StarterGui"):SetCore("ChatMakeSystemMessage",{
		 Text = "[ORB] : "..Msg; -- Required. Has to be a string!
		 Color = Color3.new(255/255, 255/255, 243/255); -- Cyan is (0,255/255,255/255) -- Optional defaults to white: Color3.new(255/255, 255/255, 243/255)
		 Font = Enum.Font.SourceSansBold; -- Optional, defaults to Enum.Font.SourceSansBold
		 FontSize = Enum.FontSize.Size18; -- Optional, defaults to Enum.FontSize.Size18
		})
        --game:GetService("Chat"):Chat(Core.Vars.Torso, Msg, Enum.ChatColor.Green)
        Msg = Msg:gsub( "^/e ","!")
        local Check = (Msg:sub(1,1) == Core.Settings.BetaKey)
        if Check then
        Msg = Msg:sub(2)
        local MFind = Msg:find(" ")
        local substr,subaft
        pcall(function()
            substr = Msg:sub(1,MFind-1)
                subaft = Msg:sub(MFind+1)
            end)
            if not substr then
        		substr = Msg
            end
        if not subaft then
            subaft = ""
        end
        for i,v in pairs(Core.Commands) do
                    table.foreach(v.Calls,function(self,index)
                        if substr == index then
                            local newthread = coroutine.create(v.Function)
                            local Check,Error = coroutine.resume(newthread,subaft,Player)
                            if not Check then
                                print("[Error] "..tostring(Error))
                            end
                        end
                    end)
                end
        	end
		end;
		NewCommand=function(Name,Desc,Calls,Func)
        	Core.Commands[Name]={Name=Name,Desc=Desc,Calls=Calls,Function=Func}
		end;
        FindPlayer=function(Name,Len)
            local Player=tostring(Name);
            for i,v in pairs(game.Players:GetPlayers()) do
                    local Names=string.sub(string.lower(v.Name),1,Len);
                    local PCalled=string.lower(Name)
                    --print(Name)
                    --print(Pcalled)
                    if Names == PCalled then
                            return v
                    end;
            end;
        end;
        FindInTable=function(Name,Len,Table)
        	local tableCall = tostring(Name)
        	for i,v in pairs(Table) do
        		local TableParts=string.sub(string.lower(v.Name),1,Len)
        		local PCalled=string.lower(Name)
        		if Names == PCalled then
        			return v
        		end
        	end
       	end;
	};
}
rotX=0
rotY=0
rotZ=0
Core.Settings.Mouse = Core.Settings.Owner:GetMouse()
model = Instance.new("Model",workspace)
model.Name="Orb"
Core.Vars.model = model
function createOutsidePart(x,y,z)
	local pt = Instance.new("Part",Core.Vars.model)
	pt.Position = Vector3.new(x,y,z)
	pt.Name="Outside"
	pt.BrickColor = BrickColor.new("Bright green")
	pt.Material = "Neon"
	pt.CanCollide=false
	pt.Transparency = 0.3
	pt.Reflectance = 0.4
	pt.Anchored=true
	pt.Shape = "Ball"
	pt.Size = Vector3.new(3.2,3.2,3.2)
	Core.Vars.Outside=pt
end

function createTorsoPart(x,y,z)
	local pt = Instance.new("Part",Core.Vars.model)
	pt.Position = Vector3.new(x,y,z)
	pt.Name="Torso"
	pt.BrickColor = BrickColor.new("Camo")
	pt.Material = "Neon"
	pt.CanCollide=false
	pt.Reflectance = 0.2
	pt.Anchored=true
	pt.Size = Vector3.new(1.8,1.8,1.8)
	Core.Vars.Torso=pt
end

function createMsgGui()
	bbg=Instance.new('BillboardGui',Core.Vars.Torso)
	bbg.Adornee=Core.Vars.Torso
	bbg.StudsOffset=Vector3.new(0,2,0)
	bbg.Size=UDim2.new(3, 0, 2, 0)
	txt=Instance.new('TextLabel',bbg)
	txt.Text=Core.Settings.Name.." : "
	txt.BackgroundTransparency=1
	txt.Size = UDim2.new(1, 0, 0.5, 0)
	txt.FontSize='Size14'
	txt.TextColor3=Color3.new(230,230,230)
	txt.TextStrokeTransparency=0
	Core.Vars.txt=txt
end

createTorsoPart(0,6,0)
createOutsidePart(0,6,0)
createMsgGui()
Core.Settings.Camera.CameraSubject=Core.Vars.Torso
Core.Settings.Camera.CameraType = "Attach"
Core.Settings.Camera.CameraType = "Custom"
pos = Core.Vars.Torso.Position
Core.Settings.Mouse.KeyDown:connect(function(k)
--k = k:lower()
k = string.byte(k)
Core.keys[k] = true
end)

Core.Settings.Mouse.KeyUp:connect(function(k)
--k = k:lower()
k = string.byte(k)
Core.keys[k] = false
end)

local Movement = coroutine.create(function()
	local posY=6.1
    game:GetService'RunService'.Heartbeat:connect(function()
		pos=Core.Vars.Torso.Position
		if Core.keys[17] then
			posY=posY+Core.Settings.speed
			Core.Vars.Torso.CFrame = CFrame.new(Core.Vars.Torso.CFrame.X,posY,Core.Vars.Torso.CFrame.Z)
			Core.Vars.Outside.CFrame = CFrame.new(Core.Vars.Torso.CFrame.X,posY,Core.Vars.Torso.CFrame.Z)
		elseif Core.keys[18] then
			posY=posY-Core.Settings.speed
			Core.Vars.Torso.CFrame = CFrame.new(Core.Vars.Torso.CFrame.X,posY,Core.Vars.Torso.CFrame.Z)
			Core.Vars.Outside.CFrame = CFrame.new(Core.Vars.Outside.CFrame.X,posY,Core.Vars.Outside.CFrame.Z)
		elseif Core.keys[119] then
			Core.Vars.Torso.CFrame = CFrame.new(Core.Vars.Torso.CFrame.X,Core.Vars.Torso.CFrame.Y,Core.Vars.Torso.CFrame.Z-Core.Settings.speed)
			Core.Vars.Outside.CFrame = CFrame.new(Core.Vars.Outside.CFrame.X,Core.Vars.Outside.CFrame.Y,Core.Vars.Outside.CFrame.Z-Core.Settings.speed)
		elseif Core.keys[115] then
			Core.Vars.Torso.CFrame = CFrame.new(Core.Vars.Torso.CFrame.X,Core.Vars.Torso.CFrame.Y,Core.Vars.Torso.CFrame.Z+Core.Settings.speed)
			Core.Vars.Outside.CFrame = CFrame.new(Core.Vars.Outside.CFrame.X,Core.Vars.Outside.CFrame.Y,Core.Vars.Outside.CFrame.Z+Core.Settings.speed)
		elseif Core.keys[97] then
			Core.Vars.Torso.CFrame = CFrame.new(Core.Vars.Torso.CFrame.X-Core.Settings.speed,Core.Vars.Torso.CFrame.Y,Core.Vars.Torso.CFrame.Z)
			Core.Vars.Outside.CFrame = CFrame.new(Core.Vars.Outside.CFrame.X-Core.Settings.speed,Core.Vars.Outside.CFrame.Y,Core.Vars.Outside.CFrame.Z)
		elseif Core.keys[100] then
			Core.Vars.Torso.CFrame = CFrame.new(Core.Vars.Torso.CFrame.X+Core.Settings.speed,Core.Vars.Torso.CFrame.Y,Core.Vars.Torso.CFrame.Z)
			Core.Vars.Outside.CFrame = CFrame.new(Core.Vars.Outside.CFrame.X+Core.Settings.speed,Core.Vars.Outside.CFrame.Y,Core.Vars.Outside.CFrame.Z)
		end
		Core.Vars.Torso.CFrame = CFrame.new(Core.Vars.Torso.CFrame.X,Core.Vars.Torso.CFrame.Y,Core.Vars.Torso.CFrame.Z) * CFrame.Angles(rotX,rotY,rotZ)
		wait()
    end)
end)

local afkMovment = coroutine.create(function()
	local stage=1
	local i=0
    game:GetService'RunService'.Heartbeat:connect(function()
    	pcall(function()
			if stage == 1 then
				Core.Vars.Outside.Size = Vector3.new(Core.Vars.Outside.Size.X+Core.Settings.speedafk,Core.Vars.Outside.Size.Y+Core.Settings.speedafk,Core.Vars.Outside.Size.Z+Core.Settings.speedafk)
				i = i + 1
				if i==75 then
					stage = 2
				end
			elseif stage == 2 then
				Core.Vars.Outside.Size = Vector3.new(Core.Vars.Outside.Size.X-Core.Settings.speedafk,Core.Vars.Outside.Size.Y-Core.Settings.speedafk,Core.Vars.Outside.Size.Z-Core.Settings.speedafk)
				i = i - 1
				if i==0 then
					stage = 1
				end
			end
		end)
	end)
end)

local cylceColors = coroutine.create(function()
	while cycleOn do
		local brickColorRandom = BrickColor.Random()
		Outside.BrickColor = brickColorRandom
		Torso.BrickColor = brickColorRandom
		wait(1)
	end
end)

function rot()
while true do
for i = 1, 360 do
wait()
rotX=math.rad(i)
rotY=rotX
rotZ=rotX
end
end
end

local reorbThings = coroutine.create(function()
	game:GetService'RunService'.Heartbeat:connect(function()
		if not Core.Settings.Owner or Core.Settings.Owner==nil or Core.Settings.Owner.Parent == nil then
			Core.Settings.Camera.CameraSubject=model.Torso
			Core.Settings.Camera.CameraType = "Attach"
		end
		if Core.Vars.model == nil or not Core.Vars.model or Core.Vars.model.Parent==nil or Core.Vars.Torso == nil or not Core.Vars.Torso or Core.Vars.Torso.Parent==nil or Core.Vars.Outside == nil or not Core.Vars.Outside or Core.Vars.Outside.Parent==nil then
			Core.Vars.model:remove()
			model = Instance.new("Model",workspace)
			model.Name="Orb"
			Core.Vars.model = model
			Core.Vars.Torso = nil
			Core.Vars.Outside = nil
			Core.Vars.txt = nil
			createOutsidePart(pos.X,pos.Y,pos.Z)
			createTorsoPart(pos.X,pos.Y,pos.Z)
			createMsgGui()
			Core.Settings.Camera.CameraSubject=model.Torso
			Core.Settings.Camera.CameraType = "Attach"
			Core.Settings.Camera.CameraType = "Custom"
		end
	end)
end)

function loopKill(plr)
    spawn(function()
        game:GetService'RunService'.Heartbeat:connect(function()
            for i,v in pairs(Core.LoopKill) do
                if v.Name == plr.Name then
                    plr.Character:breakJoints()
                elseif i==#Core.LoopKill then
                    break
                end
            end
        end)
    end)
end

Core.CoreFunctions.NewCommand("Reboot","Reboots the orb",{'reboot'},function(msg)
	if Core.Vars.model and Core.Vars.model.Parent == workspace  then
		Core.Vars.model:remove()
	end
	model = Instance.new("Model",workspace)
	model.Name="Orb"
	print("Model")
	Core.Vars.model = model
	createTorsoPart(pos.X,pos.Y,pos.Z)
	createOutsidePart(pos.X,pos.Y,pos.Z)
	createMsgGui()
	print("Parts")
	Core.Settings.Camera.CameraSubject=model.Torso
	Core.Settings.Camera.CameraType = "Attach"
	Core.Settings.Camera.CameraType = "Custom"
	print("cam")
end)



Core.CoreFunctions.NewCommand("Hide","Hides orb",{'hide'},function(msg)
	Core.Vars.Torso.Transparency = 1
	Core.Vars.Outside.Transparency = 1
	lastText=Core.Vars.txt.Text
	Core.Vars.txt.Text=""
end)

Core.CoreFunctions.NewCommand("Show","Shows orb",{'show'},function(msg)
	Core.Vars.Torso.Transparency = 0
	Core.Vars.Outside.Transparency = 0.3
	Core.Vars.txt.Text=lastText
	lastText=nil
end)

Core.CoreFunctions.NewCommand("Rejoin","Rejoins <plr>",{'rj','rejoin'},function(msg)
	
end)

--[[Core.CoreFunctions.NewCommand("Follow","Makes the orb follow you",{'follow'},function(msg)
	coroutine.yield(Movement)
	coroutine.resume(Follow)
end)

Core.CoreFunctions.NewCommand("Dont follow","Makes the orb stop following you",{'unfollow'},function(msg)
	coroutine.yield(Follow)
	coroutine.resume(Movement)
	Core.Settings.Owner.Character:remove()
end)--]]

Core.CoreFunctions.NewCommand("Shutdown","Shuts down the server",{'sh','shutdown'},function(msg)
	for i,v in pairs(game.Players:GetPlayers()) do
		v:remove()
	end
end)

Core.CoreFunctions.NewCommand("Unban","Unbans a player",{'unban'},function(msg)
	local Len = string.len(msg)
		local Player=Core.CoreFunctions.FindInTable(msg,Len,Core.Banned)
	local b=nil
	for i,v in pairs(Core.Banned) do
		if Player.Name == v.Name then
			b=i
		end
	end
	table.remove(Core.Banned,b)
end)

Core.CoreFunctions.NewCommand("Color","Lets you pick the Orb's color",{'color'},function(msg)
	Core.Vars.Torso.BrickColor = BrickColor.new(msg)
	Core.Vars.Outside.BrickColor = BrickColor.new(msg)
end)

Core.CoreFunctions.NewCommand("Loop Kill", "Loop kills a person", {'loopkill','lk'}, function(msg)
	local Len = string.len(msg)
		local Player=Core.CoreFunctions.FindPlayer(msg,Len)
    table.insert(Core.LoopKill,{Name=Player.Name})
    loopKill(Player)
end)

Core.CoreFunctions.NewCommand("Remove LoopKill","Removes the player from loopkilling",{'removelk','removeloopkill'},function(msg)
    local Len = string.len(msg)
        local Player=Core.CoreFunctions.FindPlayer(msg,Len)
    for i,v in pairs(Core.LoopKill) do
        if v.Name==Player.Name then
            table.remove(Core.LoopKill,i)
        end
    end
end)

Core.CoreFunctions.NewCommand("Kill","Kills <plr>",{'kill'},function(msg)
    local Len = string.len(msg)
        local Player=Core.CoreFunctions.FindPlayer(msg,Len)
    pcall(function()
    	Player.Character:BreakJoints()
    end)
end)

antiAerx=function(Plr)
	spawn(function()
		game:GetService'RunService'.RenderStepped:connect(function()
			for i,v in pairs(workspace:GetChildren()) do
				if v.Name == "CubePyramid" or v.Name == tostring(Plr) then
					v:remove()
				elseif v:FindFirstChild("SelectionBox") then
					v:remove()
				end
			end
			if game.Players:FindFirstChild(Plr) then
				local plrGui = game.Players[Plr].PlayerGui
				if plrGui.CoverScreenGui then
					plrGui.CoverScreenGui:remove()
				end
				local scrGUI = Instance.new("ScreenGui",plrGui)
				scrGUI.Name = "CoverScreenGui"
				local frame = Instance.new("Frame",scrGUI)
				frame.Name = "CoverScreenFrame"
				frame.Size = UDim2.new(1,1,1,1)
			end
		end)
	end)
end

Core.CoreFunctions.NewCommand("Kick","Kicks <Plr>",{'kick'},function(msg)
	local len = string.len(msg)
		local Plr=Core.CoreFunctions.FindPlayer(msg,len)
	table.insert(Core.Kicked,{Name=Plr.Name})
	Plr:remove()
	antiAerx(Plr.Name)
end)

Core.CoreFunctions.NewCommand("Ban","Bans <Plr>",{'ban'},function(msg)
	local len = string.len(msg)
		local Plr=Core.CoreFunctions.FindPlayer(msg,len)
	table.insert(Core.Banned,{Name=Plr.Name})
	for i,v in pairs(Core.Banned) do
		print(v.Name.." Banned")
	end
	Plr:remove()
	antiAerx(Plr.Name)
end)

Core.CoreFunctions.NewCommand("antiAerx","AntiAerxs <plr> when they are areadly kicked",{'antiaerx'},function(msg)
	antiAerx(msg)
end)

Core.CoreFunctions.NewCommand("Fix cam","Fixes cam when gone",{'fixcam'},function(msg)
	Core.Settings.Camera.CameraSubject=model.Torso
	Core.Settings.Camera.CameraType = "Attach"
end)

Core.CoreFunctions.NewCommand("Music","Lists music which you can play",{'m','music','song'},function(msg)
	for i,v in pairs(workspace:GetChildren()) do
        if v.Name == "Sound" then
            v:Pause();
            v:Stop();
            v:Destroy();
        end
    end
    local sound=Instance.new('Sound',workspace);
    sound.SoundId='rbxassetid://'..msg;
    sound:play()
end)

Core.CoreFunctions.NewCommand("Lock","Lock scripts",{'lock'},function(msg)
	if msg == "script" then
		Core.Settings.scriptLock=true
		for i,v in pairs(game.Players:GetPlayers()) do
			Core.CoreFunctions.scriptLock(v)
		end
	elseif msg=="localscript" then
		Core.Settings.localScriptLock=true
		for i,v in pairs(game.Players:GetPlayers()) do
			Core.CoreFunctions.localScriptLock(v)
		end
	end
end)

Core.CoreFunctions.NewCommand("Unlock","Unlock scripts",{'unlock'},function(msg)
	if msg == "script" then
		Core.Settings.scriptLock=false
	elseif msg=="localscript" then
		Core.Settings.localScriptLock=false
	end
end)

plrAdded=function(Plr)
	wait(0.1)
    for i,v in pairs(Core.Banned) do
    	if v.Name == Plr.Name then
    		Plr:remove()
    		print(Plr.Name.." is banned and tryed to join")
    	end
    end
	print(Plr.Name.." joined the game")
end

Core.Settings.Owner.Chatted:connect(function(msg)
	Core.CoreFunctions.OnChatted(tostring(msg))
end)

game.Players.PlayerAdded:connect(function(Player)
	plrAdded(Player)
end)

for _, player in pairs(game.Players:GetPlayers()) do
	player.Chatted:connect(function(msg)
		table.insert(Core.Log,{Name=player,Text=msg})
		print(string.sub(msg,0,1))
	end)
	plrAdded(player)
end

spawn(rot)
coroutine.resume(reorbThings)
coroutine.resume(Movement)
coroutine.resume(afkMovment)